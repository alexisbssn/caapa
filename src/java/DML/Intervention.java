package DML;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Alexis
 */
@Entity
@Table(name="intervention")  
public class Intervention implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idintervention")
    private int id;
    @Column(name = "description")
    private String description;
    @Column(name = "limitDate")
    private Date meetingDate;
    @Column(name = "duration")
    private int duration;
    @Column(name = "distance")
    private Double kmDistance;
    @Column(name = "address")
    private String address;
    
    @ManyToOne
    @JoinColumn(name = "idTypeIntervention")
    private InterventionType type;
    
    @ManyToOne
    @JoinColumn(name = "CreatedBy")
    private Ressource createdBy;
    
    @ManyToMany
    @JoinTable(name = "intervention_ressource",
        joinColumns=@JoinColumn(name = "idintervention"),
        inverseJoinColumns=@JoinColumn(name = "idressource"))
    @Expose(serialize = false, deserialize = true)
     private List<Ressource> ressources;
    
    @ManyToOne
    @JoinColumn(name = "idDemande")
    private Demande demande;
    
    @ManyToMany(mappedBy = "interventions")
    @Expose(serialize = false, deserialize = true)
    private List<Probleme> problemes;

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getMeetingDate() {
        return meetingDate;
    }

    public void setMeetingDate(Date meetingDate) {
        this.meetingDate = meetingDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Double getKmDistance() {
        return kmDistance;
    }

    public void setKmDistance(Double kmDistance) {
        this.kmDistance = kmDistance;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public InterventionType getType() {
        return type;
    }

    public void setType(InterventionType type) {
        this.type = type;
    }

    public Ressource getCreatedBy() {
        return createdBy;
    }

    public List<Ressource> getRessources() {
        return ressources;
    }

    public void addRessource(Ressource ressource) {
        this.ressources.add(ressource);
    }
    
    public void removeRessource(int idRessource){
        for(int i = 0; i < ressources.size(); i++){
            if(ressources.get(i).getId() == idRessource){
                ressources.remove(i);
                break;
            }
        }
    }

    public Demande getDemande() {
        return demande;
    }

    public void setDemande(Demande demande) {
        this.demande = demande;
    }

    public List<Probleme> getProblemes() {
        return problemes;
    }

    public void addProbleme(Probleme probleme) {
        this.problemes.add(probleme);
    }
    
    public void removeProbleme(int idProbleme){
        for(int i = 0; i < problemes.size(); i++){
            if(problemes.get(i).getId() == idProbleme){
                problemes.remove(i);
                break;
            }
        }
    }

    public Intervention(int id, String description, Date meetingDate, int duration, Double kmDistance, String address, InterventionType type, Ressource createdBy, List<Ressource> ressources, Demande demande) {
        this.id = id;
        this.description = description;
        this.meetingDate = meetingDate;
        this.duration = duration;
        this.kmDistance = kmDistance;
        this.address = address;
        this.type = type;
        this.createdBy = createdBy;
        this.ressources = ressources;
        this.demande = demande;
    }

    public Intervention(String description, Date meetingDate, int duration, Double kmDistance, String address, InterventionType type, Ressource createdBy, Demande demande) {
        this.description = description;
        this.meetingDate = meetingDate;
        this.duration = duration;
        this.kmDistance = kmDistance;
        this.address = address;
        this.type = type;
        this.createdBy = createdBy;
        this.demande = demande;
        this.ressources = new ArrayList();
    }

    public Intervention() {
    }
    
    public Intervention(int id) {
        this.id = id;
    }
}

package DML;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Alexis
 */
@Entity
@Table(name="interventiontype")  
public class InterventionType implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idinterventiontype")
    private int id;
    @Column(name = "label")
    private String label;
    @Column(name = "description")
    private String description;
    @Column(name = "lieu")
    private String Lieu;

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLieu() {
        return Lieu;
    }

    public void setLieu(String Lieu) {
        this.Lieu = Lieu;
    }

    public InterventionType(int id, String label, String description, String Lieu) {
        this.id = id;
        this.label = label;
        this.description = description;
        this.Lieu = Lieu;
    }

    public InterventionType(String label, String description, String Lieu) {
        this.label = label;
        this.description = description;
        this.Lieu = Lieu;
    }

    public InterventionType() {
    }
    public InterventionType(int id) {
        this.id = id;
    }
}

package DML;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Alexis
 */
@Entity
@Table(name="THEME")   
public class Theme implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idtheme")
    private int id;
    @Column(name = "label")
    private String label;
    @Column(name = "description")
    private String description;

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Theme(int id, String label, String description) {
        this.id = id;
        this.label = label;
        this.description = description;
    }
    
    public Theme(int id) {
        this.id = id;
    }

    public Theme(String label, String description) {
        this.label = label;
        this.description = description;
    }

    public Theme() {
    }
    
}
package DML;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Alexis
 */

@Entity
@Table(name="demande") 
public class Demande implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iddemande")
    private int id;
    @Column(name = "description")
    private String description;
    @Column(name = "status")
    private String status;
    
    @ManyToOne
    @JoinColumn(name = "idClient")
    private Client client;
    
    @ManyToOne
    @JoinColumn(name = "idReference")
    private Reference reference;
    
    @OneToMany(mappedBy = "demande", fetch = FetchType.LAZY, targetEntity = Probleme.class)
    @Expose(serialize = false, deserialize = false)
    private List<Probleme> problemes;
    
    @OneToMany(mappedBy = "demande", fetch = FetchType.LAZY, targetEntity = Intervention.class)
    @Expose(serialize = false, deserialize = false)
    private List<Intervention> interventions;
    
    @ManyToOne
    @JoinColumn(name = "AssignedTo")
    private Ressource assignee;
    
    @ManyToOne
    @JoinColumn(name = "CreatedBy")
    private Ressource createdBy;

    public int getId() {
        return id;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Reference getReference() {
        return reference;
    }

    public void setReference(Reference reference) {
        this.reference = reference;
    }

    public List<Probleme> getProblemes() {
        return problemes;
    }

    public void addProbleme(Probleme probleme) {
        this.problemes.add(probleme);
    }
    
    public void removeProbleme(int problemeId){
        for(int i = 0; i < problemes.size(); i++){
            if(problemes.get(i).getId() == problemeId){
                problemes.remove(i);
                break;
            }
        }
    }

    public List<Intervention> getInterventions() {
        return interventions;
    }

    public void addIntervention(Intervention intervention) {
        this.interventions.add(intervention);
    }
    
    public void removeIntervention(int interventionId){
        for(int i = 0; i < interventions.size(); i++){
            if(interventions.get(i).getId() == interventionId){
                interventions.remove(i);
                break;
            }
        }
    }

    public Ressource getAssignee() {
        return assignee;
    }

    public void setAssignee(Ressource assignee) {
        this.assignee = assignee;
    }

    public Ressource getCreatedBy() {
        return createdBy;
    }    

    public Demande(int id, String description, String status, Client client, Reference reference, List<Probleme> problemes, List<Intervention> interventions, Ressource assignee, Ressource createdBy) {
        this.id = id;
        this.description = description;
        this.status = status;
        this.client = client;
        this.reference = reference;
        this.problemes = problemes;
        this.interventions = interventions;
        this.assignee = assignee;
        this.createdBy = createdBy;
    }

    public Demande(String description, String status, Client client, Reference reference, Ressource assignee, Ressource createdBy) {
        this.description = description;
        this.status = status;
        this.client = client;
        this.reference = reference;
        this.assignee = assignee;
        this.createdBy = createdBy;
        this.interventions = new ArrayList();
        this.problemes = new ArrayList();
    }
    
    public Demande(int demandeId,String description, String status, Client client, Reference reference, Ressource assignee, Ressource createdBy) {
        this.id = demandeId;
        this.description = description;
        this.status = status;
        this.client = client;
        this.reference = reference;
        this.assignee = assignee;
        this.createdBy = createdBy;
        this.interventions = new ArrayList();
        this.problemes = new ArrayList();
    }

    public Demande() {
    }
    
    public Demande(int id) {
        this.id = id;
    }
}

package DML;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Alexis
 */
@Entity
@Table(name="ressourcetype")   
public class RessourceType implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idressourcetype")
    private int id;
    @Column(name = "label")
    private String label;

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public RessourceType(int id, String label) {
        this.id = id;
        this.label = label;
    }

    public RessourceType(String label) {
        this.label = label;
    }

    public RessourceType() {
    }

    public RessourceType(int id) {
        this.id = id;
    }
}

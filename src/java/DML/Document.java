package DML;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Alexis
 */

@Entity
@Table(name="document")  
public class Document implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iddocument")
    private int id;
    @Column(name = "title")
    private String title;
    @Column(name = "description")
    private String description;
    @Column(name = "document")
    private Byte[] contents;
    
    @ManyToMany(mappedBy = "documents")
    @Expose(serialize = false, deserialize = true)
    private List<Probleme> linkedProblemes;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Byte[] getContents() {
        return contents;
    }

    public void setContents(Byte[] contents) {
        this.contents = contents;
    }

    public List<Probleme> getLinkedProblemes() {
        return linkedProblemes;
    }

    public void addLinkedProbleme(Probleme probleme) {
        this.linkedProblemes.add(probleme);
    }
    
    public void removeLinkedProbleme(int problemeId){
        for(int i = 0; i < linkedProblemes.size(); i++){
            if(linkedProblemes.get(i).getId() == problemeId){
                linkedProblemes.remove(i);
                break;
            }
        }
    }

    public Document(int id, String title, String description, Byte[] contents, List<Probleme> linkedProblemes) {
        this.id = id;
        this.title = title;
        this.description = description;
        //this.contents = contents;
        this.linkedProblemes = linkedProblemes;
    }

    public Document(String title, String description, Byte[] contents) {
        this.title = title;
        this.description = description;
        //this.contents = contents;
        this.linkedProblemes = new ArrayList();
    }

    public Document() {
    }
    
    public Document(int id) {
        this.id = id;
    }
}

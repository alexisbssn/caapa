    package DML;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author Alexis
 */
@Entity
@Table(name="client")
@PrimaryKeyJoinColumn(name = "idclient")
public class Client extends Contact implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idclient")  // Database name
    @SerializedName("idClient") // Json name
    private int id;
    
    @Column(name = "sex")
    private String sex;
    @Column(name = "isAlone")
    private boolean alone;
    @Column(name = "maritalStatus")
    private String maritalStatus;
    @Column(name = "birthday")
    private Date birthDate;
    @Column(name = "typeHabitation")
    private String TypeHabitation;
    @Column(name = "enAttentionHabitation")
    private String EnAttenteHabitation;
    @ManyToOne
    @JoinColumn(name = "idregion", referencedColumnName = "idregion")
    private Region region;
    @ManyToOne
    @JoinColumn(name = "idContactReference1", referencedColumnName = "idcontact")
    Contact contactReference1;
    @ManyToOne
    @JoinColumn(name = "idContactReference2", referencedColumnName = "idcontact")
    Contact contactReference2;
    
    @OneToMany(mappedBy= "client",fetch = FetchType.LAZY, targetEntity = Demande.class)
    @Expose(serialize = false, deserialize = false)
    private List<Demande> demandes;

    public int getClientId() {
        return id;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public boolean isAlone() {
        return alone;
    }

    public void setAlone(boolean isAlone) {
        this.alone = isAlone;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getTypeHabitation() {
        return TypeHabitation;
    }

    public void setTypeHabitation(String TypeHabitation) {
        this.TypeHabitation = TypeHabitation;
    }

    public String getEnAttenteHabitation() {
        return EnAttenteHabitation;
    }

    public void setEnAttenteHabitation(String EnAttenteHabitation) {
        this.EnAttenteHabitation = EnAttenteHabitation;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public List<Demande> getDemandes() {
        return demandes;
    }

    public void addDemande(Demande demande) {
        this.demandes.add(demande);
    }
    
    public void removeDemande(int demandeId){
        for(int i = 0; i < demandes.size(); i++){
            if(demandes.get(i).getId() == demandeId){
                demandes.remove(i);
                break;
            }
        }
    }
    
    public int getAge(){
        
        Calendar now = new GregorianCalendar();
        Calendar birthday = new GregorianCalendar();
        birthday.setTimeInMillis(this.getBirthDate().getTime());
        
        int age = 0;
        
        if (birthday.after(now)) {
            throw new IllegalArgumentException("Can't be born in the future");
        }
        age = now.get(Calendar.YEAR) - birthday.get(Calendar.YEAR);
        
        if ((birthday.get(Calendar.DAY_OF_YEAR) - now.get(Calendar.DAY_OF_YEAR) > 3)
                || (birthday.get(Calendar.MONTH) > now.get(Calendar.MONTH))) {
            age--;
        } else if ((birthday.get(Calendar.MONTH) == now.get(Calendar.MONTH))
                && (birthday.get(Calendar.DAY_OF_MONTH) > now.get(Calendar.DAY_OF_MONTH))) {
            age--;
        }
        return age;
    }

    public Client(int clientId, String sex, boolean alone, String maritalStatus, Date birthDate, String TypeHabitation, String EnAttenteHabitation, Region region, Contact contact1, Contact contact2, List<Demande> demandes,
            int contactId, String firstName, String lastName, String address, String phone, String phone2, String email) {
        super(contactId, firstName, lastName, address, phone, phone2, email);
        this.id = clientId;
        this.sex = sex;
        this.alone = alone;
        this.maritalStatus = maritalStatus;
        this.birthDate = birthDate;
        this.TypeHabitation = TypeHabitation;
        this.EnAttenteHabitation = EnAttenteHabitation;
        this.region = region;
        this.contactReference1 = contact1;
        this.contactReference2 = contact2;
        this.demandes = demandes;
    }

    public Client(int clientID)
    {
        super();
        this.id = clientID;
    }
    
    public Client(String sex, boolean alone, String maritalStatus, Date birthDate, String TypeHabitation, String EnAttenteHabitation, Region region, Contact contact1, Contact contact2, String firstName, String lastName, String address, String phone, String phone2, String email) {
        super(firstName, lastName, address, phone, phone2, email);
        this.sex = sex;
        this.alone = alone;
        this.maritalStatus = maritalStatus;
        this.birthDate = birthDate;
        this.TypeHabitation = TypeHabitation;
        this.EnAttenteHabitation = EnAttenteHabitation;
        this.region = region;
        this.contactReference1 = contact1;
        this.contactReference2 = contact2;
        this.demandes = new ArrayList();
    }

    public Client() {
    }
}

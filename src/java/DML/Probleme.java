package DML;

import com.google.gson.annotations.Expose;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Alexis
 */
@Entity
@Table(name="problem")  
public class Probleme implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idproblem")
    private int id;
    @Column(name = "description")
    private String description;
    @Column(name = "status")
    private String status;
    @Column(name = "recommendation")
    private String possibleSolution;
    
    @ManyToOne
    @JoinColumn(name = "idTheme")
    private Theme theme;
    
    @ManyToMany(targetEntity = Document.class)
    @JoinTable(
      name="probleme_document",
      joinColumns=@JoinColumn(name="idproblem"),
      inverseJoinColumns=@JoinColumn(name="iddocument"))
    @Expose(serialize = false, deserialize = true)
    private List<Document> documents;
    
    @ManyToMany(targetEntity = Probleme.class, cascade = CascadeType.PERSIST)
    @JoinTable(
      name="problem_problem",
      joinColumns=@JoinColumn(name="idproblem1", referencedColumnName="idproblem"),
      inverseJoinColumns=@JoinColumn(name="idproblem2", referencedColumnName="idproblem"))
    @Expose(serialize = false, deserialize = true)
    private List<Probleme> linkedProblems;
    
    @ManyToMany(targetEntity = Intervention.class, cascade = CascadeType.PERSIST)
    @JoinTable(
      name="probleme_intervention",
      joinColumns=@JoinColumn(name="idprobleme", referencedColumnName="idproblem"),
      inverseJoinColumns=@JoinColumn(name="idintervention", referencedColumnName="idintervention"))
    @Expose(serialize = false, deserialize = true)
    private List<Intervention> interventions;
    
    @ManyToOne
    @JoinColumn(name = "CreatedBy")
    private Ressource createdBy;
    
    @ManyToOne
    @JoinColumn(name = "idDemande")
    private Demande demande;

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPossibleSolution() {
        return possibleSolution;
    }

    public void setPossibleSolution(String possibleSolution) {
        this.possibleSolution = possibleSolution;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void addDocuments(Document document) {
        this.documents.add(document);
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }
    
    public void removeDocument(int documentId){
        for(int i = 0; i < documents.size(); i++){
            if(documents.get(i).getId() == documentId){
                documents.remove(i);
                break;
            }
        }
    }

    public List<Probleme> getLinkedProblems() {
        return linkedProblems;
    }

    public void addLinkedProblems(Probleme probleme) {
        this.linkedProblems.add(probleme);
    }
    
    public void removeLinkedProbleme(int problemeId){
        for(int i = 0; i < linkedProblems.size(); i++){
            if(linkedProblems.get(i).getId() == problemeId){
                linkedProblems.remove(i);
                break;
            }
        }
    }

    public List<Intervention> getInterventions() {
        return interventions;
    }

    public void addInterventions(Intervention intervention) {
        this.interventions.add(intervention);
    }
    
    public void removeIntervention(int interventionId){
        for(int i = 0; i < interventions.size(); i++){
            if(interventions.get(i).getId() == interventionId){
                interventions.remove(i);
                break;
            }
        }
    }

    public Ressource getCreatedBy() {
        return createdBy;
    }

    public Demande getDemande() {
        return demande;
    }

    public void setDemande(Demande demande) {
        this.demande = demande;
    }

    public Probleme(int id, String description, String status, String possibleSolution, List<Document> documents, List<Probleme> linkedProblems, List<Intervention> interventions, Ressource createdBy,Theme theme, Demande demande) {
        this.id = id;
        this.description = description;
        this.status = status;
        this.possibleSolution = possibleSolution;
        this.documents = documents;
        this.linkedProblems = linkedProblems;
        this.interventions = interventions;
        this.createdBy = createdBy;
        this.theme = theme;
        this.demande = demande;
    }

    public Probleme(String description, String status, String possibleSolution, Ressource createdBy,Theme theme, Demande demande) {
        this.description = description;
        this.status = status;
        this.possibleSolution = possibleSolution;
        this.createdBy = createdBy;
        this.theme = theme;
        this.demande = demande;
        this.documents = new ArrayList();
        this.interventions = new ArrayList();
        this.linkedProblems = new ArrayList();
    }
    
    public Probleme(int id, String description, String status, String possibleSolution, Ressource createdBy,Theme theme, Demande demande) {
        this.id = id;
        this.description = description;
        this.status = status;
        this.possibleSolution = possibleSolution;
        this.createdBy = createdBy;
        this.theme = theme;
        this.demande = demande;
        this.documents = new ArrayList();
        this.interventions = new ArrayList();
        this.linkedProblems = new ArrayList();
    }

    public Probleme() {
    }
    
    public Probleme(int id) {
        this.id = id;
    }
}

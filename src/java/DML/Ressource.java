package DML;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 *
 * @author Alexis
 */

@Entity
@Table(name="ressource")
@PrimaryKeyJoinColumn(name = "idressource")
public class Ressource extends Contact implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idressource") // Database name
    @SerializedName("idRessource") // Json name
    private int id;
    @Column(name = "login")
    private String login;
    @Column(name = "password")
    private String passwordHash;

    @ManyToOne
    @JoinColumn(name = "idRessourceType")
     private RessourceType type;
    
    @ManyToOne
    @JoinColumn(name = "idRegion")
     private Region region;
    
    @ManyToOne
    @JoinColumn(name = "createdBy")
     private Ressource createdBy;
    
    @OneToMany(mappedBy = "assignee")
    private List<Demande> demandes;

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public RessourceType getType() {
        return type;
    }

    public void setType(RessourceType type) {
        this.type = type;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Ressource getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Ressource createdBy) {
        this.createdBy = createdBy;
    }

    public List<Demande> getDemandes() {
        return demandes;
    }

    public void addDemande(Demande demande) {
        this.demandes.add(demande);
    }
    
    public void removeDemande(int demandeId){
        for(int i = 0; i < demandes.size(); i++){
            if(demandes.get(i).getId() == demandeId){
                demandes.remove(i);
                break;
            }
        }
    }

    public Ressource(int ressourceId, String login, String passwordHash, RessourceType type, Region region, Ressource createdBy, List<Demande> demandes,
            int contactId, String firstName, String lastName, String address, String phone, String phone2, String email) {
        super(contactId, firstName, lastName, address, phone, phone2, email);
        this.id = ressourceId;
        this.login = login;
        this.passwordHash = passwordHash;
        this.type = type;
        this.region = region;
        this.createdBy = createdBy;
        this.demandes = demandes;
    }

    public Ressource(int ressourceId)
    {
        super();
        this.id = ressourceId;
    }
    
    public Ressource(String login, String passwordHash, RessourceType type, Region region, Ressource createdBy, String firstName, String lastName, String address, String phone, String phone2, String email) {
        super(firstName, lastName, address, phone, phone2, email);
        this.login = login;
        this.passwordHash = passwordHash;
        this.type = type;
        this.region = region;
        this.createdBy = createdBy;
        this.demandes = new ArrayList();
    }

    public Ressource() {
    }
    
}

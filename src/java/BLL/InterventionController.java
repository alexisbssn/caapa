/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.Repository;
import DML.Intervention;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pepe
 */
public class InterventionController {
    
    public static Intervention findInterventionByID(int id)
    {
        try {
            return Repository.getInstance().findInterventionByID(id);
        } catch (Exception ex) {
            Logger.getLogger(InterventionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<Intervention> listInterventions(int firstPosition, int numberOfRecords)
    {
        try {
            return Repository.getInstance().listInterventions(firstPosition, numberOfRecords);
        } catch (Exception ex) {
            Logger.getLogger(InterventionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static boolean addIntervention(Intervention intervention) 
    {
        try {
            return Repository.getInstance().addIntervention(intervention);
        } catch (Exception ex) {
            Logger.getLogger(InterventionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean removeIntervention(Intervention intervention)
    {
        try {
            return Repository.getInstance().removeIntervention(intervention);
        } catch (Exception ex) {
            Logger.getLogger(InterventionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean editIntervention(Intervention intervention)
    {
        try {
            return Repository.getInstance().editIntervention(intervention);
        } catch (Exception ex) {
            Logger.getLogger(InterventionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static List<Intervention> findInterventionByDescription(String description)
    {
        try {
            return Repository.getInstance().findInterventionByDescription(description);
        } catch (Exception ex) {
            Logger.getLogger(InterventionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

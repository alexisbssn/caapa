/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.Repository;
import DML.RessourceType;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pepe
 */
public class RessourceTypeController {
    
    public static RessourceType findRessourceTypeByID(int id) {
        try {
            return Repository.getInstance().findRessourceTypeByID(id);
        } catch (Exception ex) {
            Logger.getLogger(RessourceTypeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<RessourceType> listRessourceTypes(int firstPosition, int numberOfRecords) {
        try {
            return Repository.getInstance().listRessourceTypes(firstPosition, numberOfRecords);
        } catch (Exception ex) {
            Logger.getLogger(RessourceTypeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static boolean addRessourceType(RessourceType ressourceType) {
        try {
            return Repository.getInstance().addRessourceType(ressourceType);
        } catch (Exception ex) {
            Logger.getLogger(RessourceTypeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean removeRessourceType(RessourceType ressourceType) {
        try {
            return Repository.getInstance().removeRessourceType(ressourceType);
        } catch (Exception ex) {
            Logger.getLogger(RessourceTypeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean editRessourceType(RessourceType ressourceType){
        try {
            return Repository.getInstance().editRessourceType(ressourceType);
        } catch (Exception ex) {
            Logger.getLogger(RessourceTypeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static List<RessourceType> findRessourceTypeByLabel(String label) {
        try {
            return Repository.getInstance().findRessourceTypeByLabel(label);
        } catch (Exception ex) {
            Logger.getLogger(RessourceTypeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

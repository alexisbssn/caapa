/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.Repository;
import DML.Theme;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pepe
 */
public class ThemeController {
    public static Theme findThemeByID(int id) {
        try {
            return Repository.getInstance().findThemeByID(id);
        } catch (Exception ex) {
            Logger.getLogger(ThemeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<Theme> listThemes(int firstPosition, int numberOfRecords) {
        try {
            return Repository.getInstance().listThemes(firstPosition, numberOfRecords);
        } catch (Exception ex) {
            Logger.getLogger(ThemeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static boolean addTheme(Theme theme) {
        try {
            return Repository.getInstance().addTheme(theme);
        } catch (Exception ex) {
            Logger.getLogger(ThemeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean removeTheme(Theme theme) {
        try {
            return Repository.getInstance().removeTheme(theme);
        } catch (Exception ex) {
            Logger.getLogger(ThemeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean editTheme(Theme theme) {
        try {
            return Repository.getInstance().editTheme(theme);
        } catch (Exception ex) {
            Logger.getLogger(ThemeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static List<Theme> findThemeByLabel(String label) {
    
        try {
            return Repository.getInstance().findThemeByLabel(label);
        } catch (Exception ex) {
            Logger.getLogger(ThemeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.Repository;
import DML.Document;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pepe
 */
public class DocumentController {
    
    public static Document findDocumentByID(int id) 
    {
        try {
            return Repository.getInstance().findDocumentByID(id);
        } catch (Exception ex) {
            Logger.getLogger(DocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<Document> listDocuments(int firstPosition, int numberOfRecords)
    {
        try {
            return Repository.getInstance().listDocuments(firstPosition, numberOfRecords);
        } catch (Exception ex) {
            Logger.getLogger(DocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static boolean addDocument(Document document)
    {
        try {
            return Repository.getInstance().addDocument(document);
        } catch (Exception ex) {
            Logger.getLogger(DocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean removeDocument(Document document)
    {
        try {
            return Repository.getInstance().removeDocument(document);
        } catch (Exception ex) {
            Logger.getLogger(DocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean editDocument(Document document)
    {
        try {
            return Repository.getInstance().editDocument(document);
        } catch (Exception ex) {
            Logger.getLogger(DocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static List<Document> findDocumentByTitle(String title)
    {
        try {
            return Repository.getInstance().findDocumentByTitle(title);
        } catch (Exception ex) {
            Logger.getLogger(DocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<Document> findDocumentByDescription(String description)
    {
        try {
            return Repository.getInstance().findDocumentByDescription(description);
        } catch (Exception ex) {
            Logger.getLogger(DocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}


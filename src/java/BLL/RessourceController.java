/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.Repository;
import DML.Ressource;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pepe
 */
public class RessourceController {
    
    public static Ressource findRessourceByID(int id)
    {
        try {
            return Repository.getInstance().findRessourceByID(id);
        } catch (Exception ex) {
            Logger.getLogger(RessourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<Ressource> listRessources(int firstPosition, int numberOfRecords) {
        try {
            return Repository.getInstance().listRessources(firstPosition, numberOfRecords);
        } catch (Exception ex) {
            Logger.getLogger(RessourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static boolean addRessource(Ressource ressource)
    {
        try {
            return Repository.getInstance().addRessource(ressource);
        } catch (Exception ex) {
            Logger.getLogger(RessourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean removeRessource(Ressource ressource)
    {
        try {
            return Repository.getInstance().removeRessource(ressource);
        } catch (Exception ex) {
            Logger.getLogger(RessourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean editRessource(Ressource ressource)
    {
        try {
            return Repository.getInstance().editRessource(ressource);
        } catch (Exception ex) {
            Logger.getLogger(RessourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static List<Ressource> findRessourceByLastName(String name) {
        try {
            return Repository.getInstance().findRessourceByLastName(name);
        } catch (Exception ex) {
            Logger.getLogger(RessourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<Ressource> findRessourceByFirstName(String name)
    {
        try {
            return Repository.getInstance().findRessourceByFirstName(name);
        } catch (Exception ex) {
            Logger.getLogger(RessourceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

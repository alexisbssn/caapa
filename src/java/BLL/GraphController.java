package BLL;

import DAL.Repository;
import DML.Client;
import DML.Demande;
import DML.Intervention;
import DML.Probleme;
import DML.Ressource;
import DTO.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author alexis
 */
public class GraphController {
    
    /**
     * Gets the number of interventions for every intervention type
     * @return DTO list (type and int)
     */
    public List<InterventionsPerLocationType_DTO> getInterventionByTypeGraph(){
        List<Intervention> interventions = getAllInterventions();
        if(interventions == null){
            return null;
        }
        
        List<InterventionsPerLocationType_DTO> dtoList = new ArrayList();
        for(Intervention i : interventions){
            boolean found = false;
            for(InterventionsPerLocationType_DTO dto : dtoList){
                if(dto.getInterventionType().equals(i.getType())){
                    dto.setNumberOfInterventions(dto.getNumberOfInterventions() + 1);
                    found = true;
                    break;
                }
            }
            if(!found){
                dtoList.add(new InterventionsPerLocationType_DTO(i.getType(), 1));
            }
        }
        return dtoList;
    }
    
    /**
     * Gets the number of interventions for every region that implicated resources are in.
     * @return DTO list (region and int)
     */
    public List<InterventionsPerRegion_DTO> getInterventionByRegionGraph(){
        List<Intervention> interventions = getAllInterventions();
        if(interventions == null){
            return null;
        }
        
        List<InterventionsPerRegion_DTO> dtoList = new ArrayList();
        for(Intervention i : interventions){
            for(Ressource r : i.getRessources()){
                boolean found = false;
                for(InterventionsPerRegion_DTO dto : dtoList){
                    if(dto.getRegion().equals(r.getRegion())){
                        dto.setNumberOfInterventions(dto.getNumberOfInterventions() + 1);
                        found = true;
                        break;
                    }
                }
                if(!found){
                    dtoList.add(new InterventionsPerRegion_DTO(1, r.getRegion()));
                }
            }
        }
        return dtoList;
    }
    
    /**
     * Gets the number of interventions for every Theme that linked problems have
     * @return DTO list (theme and int)
     */
    public List<InterventionsPerTheme_DTO> getInterventionByThemeGraph(){
        List<Intervention> interventions = getAllInterventions();
        if(interventions == null){
            return null;
        }
        
        List<InterventionsPerTheme_DTO> dtoList = new ArrayList();
        for(Intervention i : interventions){
            for(Probleme p : i.getProblemes()){
                boolean found = false;
                for(InterventionsPerTheme_DTO dto : dtoList){
                    if(dto.getTheme().equals(p.getTheme())){
                        dto.setNumberOfInterventions(dto.getNumberOfInterventions() + 1);
                        found = true;
                        break;
                    }
                }
                if(!found){
                    dtoList.add(new InterventionsPerTheme_DTO(1, p.getTheme()));
                }
            }
        }
        return dtoList;        
    }
    
    /**
     * Gets the number of interventions for every given period. The date is the start of the period.
     * Calculated from Jan 1st 2000, to 2 years after the current date.
     * @param periodFlag year: 0, quarter: 1, month: 2, week: 3
     * @return DTO list (date and int)
     */
    public List<InterventionsPerPeriods_DTO> getInterventionByPeriodGraph(int periodFlag){
        GregorianCalendar myDate = new GregorianCalendar(2000, 0, 1); //Jan 1st, 2000
        GregorianCalendar now = new GregorianCalendar();
        
        if(now.before(myDate)){
            Logger.getLogger(GraphController.class.getName()).log(Level.SEVERE, "Current date is before Jan 1st 2000. Cannot calculate periods properly in GetInterventionByPeriodGraph.");
            return null;
        }
        
        int calendarAdditionFlag;
        int calendarAdditionValue;
        switch(periodFlag){
            case 0: 
                calendarAdditionFlag = Calendar.YEAR;
                calendarAdditionValue = 1;
                break;
            case 1:
                calendarAdditionFlag = Calendar.MONTH;
                calendarAdditionValue = 3;
                break;
            case 2:
                calendarAdditionFlag = Calendar.MONTH;
                calendarAdditionValue = 1;
                break;
            case 3:
                calendarAdditionFlag = Calendar.WEEK_OF_YEAR;
                calendarAdditionValue = 1;
                break;
            default:
                Logger.getLogger(GraphController.class.getName()).log(Level.SEVERE, "Got wrong period flag in getInterventionByPeriodGraph.");
                return null;
        }
        
        List<Intervention> interventions = getAllInterventions();
        if(interventions == null){
            return null;
        }
        
        // To account for planned interventions, add 2 years
        now.add(Calendar.YEAR, 2);
        
        List<InterventionsPerPeriods_DTO> dtoList = new ArrayList();
        
        // Iterate through every time period
        while(myDate.before(now)){
            Date lowerLimit = new Date(myDate.getTimeInMillis());
            myDate.add(calendarAdditionFlag, calendarAdditionValue);
            Date upperLimit = new Date(myDate.getTimeInMillis());
            
            InterventionsPerPeriods_DTO thisTimePeriodDto = new InterventionsPerPeriods_DTO(0, lowerLimit);
            Iterator<Intervention> it = interventions.iterator();
            while(it.hasNext()){
                Intervention i = it.next();
                if(i.getMeetingDate().after(lowerLimit) || i.getMeetingDate().equals(lowerLimit) && i.getMeetingDate().before(upperLimit)){
                    thisTimePeriodDto.setNumberOfInterventions(thisTimePeriodDto.getNumberOfInterventions() + 1);
                    it.remove();
                }
            }
            if(thisTimePeriodDto.getNumberOfInterventions() != 0){
                dtoList.add(thisTimePeriodDto);
            }
        }
        return dtoList;
    }
    
    private List<Intervention> getAllInterventions(){
        try {
            return Repository.getInstance().listInterventions(0, Integer.MAX_VALUE);
        } catch (Exception ex) {
            Logger.getLogger(GraphController.class.getName()).log(Level.SEVERE, "Failed to list all interventions!", ex);
            return null;
        }
    }
    
    private List<Client> getAllClients(){
        try {
            return Repository.getInstance().listClients(0, Integer.MAX_VALUE);
        } catch (Exception ex) {
            Logger.getLogger(GraphController.class.getName()).log(Level.SEVERE, "Failed to list all clients!", ex);
            return null;
        }
    }
    
    
    /**
     * Author : JFortin
     */
    public float getTotalOfInterventionHours() throws Exception {
        int minutes = 0;
        for (Intervention intervention : getAllInterventions()) {
            minutes += intervention.getDuration();
        }
        return (float) minutes / 60;
    }

    /**
     * Author : JFortin
     */
    public double getTotalOfDistance() throws Exception {
        double km = 0;
        for (Intervention intervention : getAllInterventions()) {
            km += intervention.getKmDistance();
        }
        return km;
    }

    /**
     * Author : JFortin
     */
    public int getAverageAgeOfClients() throws Exception {
        int totalAge = 0;
        List<Client> clients = getAllClients();
        for (Client client : clients) {
            totalAge += client.getAge();
        }
        return Math.round(totalAge / clients.size());
    }

    /**
     * Author : JFortin
     */
    public List<Client_DTO> list_ClientDTOs() throws Exception {
        List<Client_DTO> result = new ArrayList<>();
        for (Client client : getAllClients()) {
            for (Demande demande : client.getDemandes()) {
                for (Intervention intervention : demande.getInterventions()) {
                    if (result.isEmpty()) {
                        result.add(new Client_DTO(client, intervention.getDuration(), 1));
                    } else {
                        boolean isFound = false;
                        for (Client_DTO dto : result) {
                            if (client.equals(dto.getClient())) {
                                float temps = (float) dto.getHoursOfInterventions() + (intervention.getDuration() / 60);
                                int interventions = dto.getNumberOfInterventions() +1;
                                dto.setHoursOfInterventions(temps);
                                dto.setNumberOfInterventions(interventions);
                                isFound = true;
                            }
                        }
                        if (!isFound) {
                            result.add(new Client_DTO(client, (float) intervention.getDuration() / 60, 1));
                        }
                    }
                }
            }
        }
        return result;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.Repository;
import DML.Probleme;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pepe
 */
public class ProblemController {
    
    public static Probleme findProblemeByID(int id)
    {
        try {
            return Repository.getInstance().findProblemeByID(id);
        } catch (Exception ex) {
            Logger.getLogger(ProblemController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<Probleme> listProblemes(int firstPosition, int numberOfRecords)
    {
        try {
            return Repository.getInstance().listProblemes(firstPosition, numberOfRecords);
        } catch (Exception ex) {
            Logger.getLogger(ProblemController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static boolean addProbleme(Probleme probleme)
    {
        try {
            return Repository.getInstance().addProbleme(probleme);
        } catch (Exception ex) {
            Logger.getLogger(ProblemController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean removeProbleme(Probleme probleme)
    {
        try {
            return Repository.getInstance().removeProbleme(probleme);
        } catch (Exception ex) {
            Logger.getLogger(ProblemController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean editProbleme(Probleme probleme)
    {
        try {
            return Repository.getInstance().editProbleme(probleme);
        } catch (Exception ex) {
            Logger.getLogger(ProblemController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static List<Probleme> findProblemeByDescription(String description)
    {
        try {
            return Repository.getInstance().findProblemeByDescription(description);
        } catch (Exception ex) {
            Logger.getLogger(ProblemController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

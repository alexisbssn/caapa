/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.Repository;
import DML.Demande;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pepe
 */
public class DemandeController {
    
    public static Demande findDemandeByID(int id)
    {
        try {
            return Repository.getInstance().findDemandeByID(id);
        } catch (Exception ex) {
            Logger.getLogger(DemandeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<Demande> listDemandes(int firstPosition, int numberOfRecords)
    {
        try {
            return Repository.getInstance().listDemandes(firstPosition, numberOfRecords);
        } catch (Exception ex) {
            Logger.getLogger(DemandeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static boolean addDemande(Demande demande)
    {
        try {
            return Repository.getInstance().addDemande(demande);
        } catch (Exception ex) {
            Logger.getLogger(DemandeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean removeDemande(Demande demande)
    {
        try {
            return Repository.getInstance().removeDemande(demande);
        } catch (Exception ex) {
            Logger.getLogger(DemandeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean editDemande(Demande demande)
    {
        try {
            return Repository.getInstance().editDemande(demande);
        } catch (Exception ex) {
            Logger.getLogger(DemandeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static List<Demande> findDemandeByDescription(String description)
    {
        try {
            return Repository.getInstance().findDemandeByDescription(description);
        } catch (Exception ex) {
            Logger.getLogger(DemandeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

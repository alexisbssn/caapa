/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.Repository;
import DML.Contact;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pepe
 */
public class ContactController {
    
    public static Contact findContactByID(int id)
    {
        try {
            return Repository.getInstance().findContactByID(id);
        } catch (Exception ex) {
            Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<Contact> listContacts(int firstPosition, int numberOfRecords)
    {
        try {
            return Repository.getInstance().listContacts(firstPosition, numberOfRecords);
        } catch (Exception ex) {
            Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static boolean addContact(Contact contact) {
    
        try {
            return Repository.getInstance().addContact(contact);
        } catch (Exception ex) {
            Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean removeContact(Contact contact) 
    {
        try {
            return Repository.getInstance().removeContact(contact);
        } catch (Exception ex) {
            Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean editContact(Contact contact)
    {
        try {
            return Repository.getInstance().editContact(contact);
        } catch (Exception ex) {
            Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static List<Contact> findContactByFirstName(String name)
    {
        try {
            return Repository.getInstance().findContactByFirstName(name);
        } catch (Exception ex) {
            Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<Contact> findContactByLastName(String name)
    {
        try {
            return Repository.getInstance().findContactByLastName(name);
        } catch (Exception ex) {
            Logger.getLogger(ContactController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

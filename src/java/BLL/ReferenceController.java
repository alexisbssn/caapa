/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.Repository;
import DML.Reference;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pepe
 */
public class ReferenceController {
    
    public static Reference findReferenceByID(int id)
    {
        try {
            return Repository.getInstance().findReferenceByID(id);
        } catch (Exception ex) {
            Logger.getLogger(ReferenceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<Reference> listReferences(int firstPosition, int numberOfRecords){
        
        try {
            return Repository.getInstance().listReferences(firstPosition, numberOfRecords);
        } catch (Exception ex) {
            Logger.getLogger(ReferenceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static boolean addReference(Reference reference)
    {
        try {
            return Repository.getInstance().addReference(reference);
        } catch (Exception ex) {
            Logger.getLogger(ReferenceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean removeReference(Reference reference) {
        try {
            return Repository.getInstance().removeReference(reference);
        } catch (Exception ex) {
            Logger.getLogger(ReferenceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean editReference(Reference reference) {
        
        try {
            return Repository.getInstance().editReference(reference);
        } catch (Exception ex) {
            Logger.getLogger(ReferenceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static List<Reference> findReferenceByDescription(String description)
    {
        try {
            return Repository.getInstance().findReferenceByDescription(description);
        } catch (Exception ex) {
            Logger.getLogger(ReferenceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

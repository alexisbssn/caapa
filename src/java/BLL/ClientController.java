/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.Repository;
import DML.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pepe
 */
public class ClientController {
    
    public static Client findClientByID(int id)
    {
        try {
            return Repository.getInstance().findClientByID(id);
        } catch (Exception ex) {
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<Client> listClients(int firstPosition, int numberOfRecords)
    {
        try {
            return Repository.getInstance().listClients(firstPosition, numberOfRecords);
        } catch (Exception ex) {
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static boolean addClient(Client client)
    {
        try {
            return Repository.getInstance().addClient(client);
        } catch (Exception ex) {
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean removeClient(Client client)
    {
        try {
            return Repository.getInstance().removeClient(client);
        } catch (Exception ex) {
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean editClient(Client client)
    {
        try {
            return Repository.getInstance().editClient(client);
        } catch (Exception ex) {
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static List<Client> findClientByLastName(String name)
    {
        try {
            return Repository.getInstance().findClientByLastName(name);
        } catch (Exception ex) {
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<Client> findClientByFirstName(String name)
    {
        try {
            return Repository.getInstance().findClientByFirstName(name);
        } catch (Exception ex) {
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

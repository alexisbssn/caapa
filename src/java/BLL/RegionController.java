/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.Repository;
import DML.Region;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pepe
 */
public class RegionController {
    
    public static Region findRegionByID(int id)
    {
        try {
            return Repository.getInstance().findRegionByID(id);
        } catch (Exception ex) {
            Logger.getLogger(RegionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<Region> listRegions(int firstPosition, int numberOfRecords)
    {
        try {
            return Repository.getInstance().listRegions(firstPosition, numberOfRecords);
        } catch (Exception ex) {
            Logger.getLogger(RegionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static boolean addRegion(Region region)
    {
        try {
            return Repository.getInstance().addRegion(region);
        } catch (Exception ex) {
            Logger.getLogger(RegionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean removeRegion(Region region)
    {
        try {
            return Repository.getInstance().removeRegion(region);
        } catch (Exception ex) {
            Logger.getLogger(RegionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean editRegion(Region region)
    {
        try {
            return Repository.getInstance().editRegion(region);
        } catch (Exception ex) {
            Logger.getLogger(RegionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static List<Region> findRegionByName(String name)
    {
        try {
            return Repository.getInstance().findRegionByName(name);
        } catch (Exception ex) {
            Logger.getLogger(RegionController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

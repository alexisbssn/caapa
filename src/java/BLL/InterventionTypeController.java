/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL;

import DAL.Repository;
import DML.InterventionType;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pepe
 */
public class InterventionTypeController {
    
    public static InterventionType findInterventionTypeByID(int id)
    {
        try {
            return Repository.getInstance().findInterventionTypeByID(id);
        } catch (Exception ex) {
            Logger.getLogger(InterventionTypeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static List<InterventionType> listInterventionTypes(int firstPosition, int numberOfRecords)
    {
        try {
            return Repository.getInstance().listInterventionTypes(firstPosition, numberOfRecords);
        } catch (Exception ex) {
            Logger.getLogger(InterventionTypeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static boolean addInterventionType(InterventionType interventionType)
    {
        try {
            return Repository.getInstance().addInterventionType(interventionType);
        } catch (Exception ex) {
            Logger.getLogger(InterventionTypeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean removeInterventionType(InterventionType interventionType){
        try {
            return Repository.getInstance().removeInterventionType(interventionType);
        } catch (Exception ex) {
            Logger.getLogger(InterventionTypeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean editInterventionType(InterventionType interventionType){
        try {
            return Repository.getInstance().editInterventionType(interventionType);
        } catch (Exception ex) {
            Logger.getLogger(InterventionTypeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static List<InterventionType> findInterventionTypeByLabel(String label) 
    {
        try {
            return Repository.getInstance().findInterventionTypeByLabel(label);
        } catch (Exception ex) {
            Logger.getLogger(InterventionTypeController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

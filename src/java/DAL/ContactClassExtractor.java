/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DML.Client;
import DML.Contact;
import DML.Ressource;
import org.eclipse.persistence.descriptors.ClassExtractor;
import org.eclipse.persistence.sessions.Record;
import org.eclipse.persistence.sessions.Session;

/**
 *
 * @author alexis
 * 
 * ONLY USEFUL WITH ECLIPSELINK
 * CANNOT SWITCH TO HIBERNATE
 */
public class ContactClassExtractor extends ClassExtractor{

    @Override
    public Class extractClassFromRow(Record record, Session sn) {
        if(record.get("idclient") != null){
            return Client.class;
        }
        if(record.get("idressource") != null) {
            return Ressource.class;
        }
        return Contact.class;
    }
    
}

package DAL;

import DML.*;
import java.util.List;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author Pepe
 */
public class Repository implements IRepository {

    private static Repository repository = null;
    private IDAO dao = null;
    
    public static Repository getInstance() throws Exception {
        if (repository == null) {
           repository = new Repository();
        }
        return repository;
    }

    private Repository() {
        String daoLookupName = "java:module/JpaDAO";
        try{
            dao = (IDAO) InitialContext.doLookup(daoLookupName);
        }catch(NamingException ne){
            Logger.getLogger("Repository").severe("Wrong persistence unit lookup name: " + daoLookupName);
        }
    }

    @Override
    public Client findClientByID(int id) {
        return dao.findClientByID(id);
    }

    @Override
    public List<Client> listClients(int firstPosition, int numberOfRecords) {
        return dao.listClients(firstPosition, numberOfRecords);
    }

    @Override
    public boolean addClient(Client client) {
        return dao.addClient(client);
    }

    @Override
    public boolean removeClient(Client client) {
        return dao.removeClient(client);
    }

    @Override
    public boolean editClient(Client client) {
        return dao.editClient(client);
    }

    @Override
    public Contact findContactByID(int id) {
        return dao.findContactByID(id);
    }

    @Override
    public List<Contact> listContacts(int firstPosition, int numberOfRecords) {
        return dao.listContacts(firstPosition, numberOfRecords);
    }

    @Override
    public boolean addContact(Contact contact) {
        return dao.addContact(contact);
    }

    @Override
    public boolean removeContact(Contact contact) {
        return dao.removeContact(contact);
    }

    @Override
    public boolean editContact(Contact contact) {
        return dao.editContact(contact);
    }

    @Override
    public Demande findDemandeByID(int id) {
        return dao.findDemandeByID(id);
    }

    @Override
    public List<Demande> listDemandes(int firstPosition, int numberOfRecords) {
        return dao.listDemandes(firstPosition, numberOfRecords);
    }

    @Override
    public boolean addDemande(Demande demande) {
        return dao.addDemande(demande);
    }

    @Override
    public boolean removeDemande(Demande demande) {
        return dao.removeDemande(demande);
    }

    @Override
    public boolean editDemande(Demande demande) {
        return dao.editDemande(demande);
    }

    @Override
    public Document findDocumentByID(int id) {
        return dao.findDocumentByID(id);
    }

    @Override
    public List<Document> listDocuments(int firstPosition, int numberOfRecords) {
        return dao.listDocuments(firstPosition, numberOfRecords);
    }

    @Override
    public boolean addDocument(Document document) {
        return dao.addDocument(document);
    }

    @Override
    public boolean removeDocument(Document document) {
        return dao.removeDocument(document);
    }

    @Override
    public boolean editDocument(Document document) {
        return dao.editDocument(document);
    }

    @Override
    public Intervention findInterventionByID(int id) {
        return dao.findInterventionByID(id);
    }

    @Override
    public List<Intervention> listInterventions(int firstPosition, int numberOfRecords) {
        return dao.listInterventions(firstPosition, numberOfRecords);
    }

    @Override
    public boolean addIntervention(Intervention intervention) {
        return dao.addIntervention(intervention);
    }

    @Override
    public boolean removeIntervention(Intervention intervention) {
        return dao.removeIntervention(intervention);
    }

    @Override
    public boolean editIntervention(Intervention intervention) {
        return dao.editIntervention(intervention);
    }

    @Override
    public InterventionType findInterventionTypeByID(int id) {
        return dao.findInterventionTypeByID(id);
    }

    @Override
    public List<InterventionType> listInterventionTypes(int firstPosition, int numberOfRecords) {
        return dao.listInterventionTypes(firstPosition, numberOfRecords);
    }

    @Override
    public boolean addInterventionType(InterventionType interventionType) {
        return dao.addInterventionType(interventionType);
    }

    @Override
    public boolean removeInterventionType(InterventionType interventionType) {
        return dao.removeInterventionType(interventionType);
    }

    @Override
    public boolean editInterventionType(InterventionType interventionType) {
        return dao.editInterventionType(interventionType);
    }

    @Override
    public Probleme findProblemeByID(int id) {
        return dao.findProblemeByID(id);
    }

    @Override
    public List<Probleme> listProblemes(int firstPosition, int numberOfRecords) {
        return dao.listProblemes(firstPosition, numberOfRecords);
    }

    @Override
    public boolean addProbleme(Probleme probleme) {
        return dao.addProbleme(probleme);
    }

    @Override
    public boolean removeProbleme(Probleme probleme) {
        return dao.removeProbleme(probleme);
    }

    @Override
    public boolean editProbleme(Probleme probleme) {
        return dao.editProbleme(probleme);
    }

    @Override
    public Reference findReferenceByID(int id) {
        return dao.findReferenceByID(id);
    }

    @Override
    public List<Reference> listReferences(int firstPosition, int numberOfRecords) {
        return dao.listReferences(firstPosition, numberOfRecords);
    }

    @Override
    public boolean addReference(Reference reference) {
        return dao.addReference(reference);
    }

    @Override
    public boolean removeReference(Reference reference) {
        return dao.removeReference(reference);
    }

    @Override
    public boolean editReference(Reference reference) {
        return dao.editReference(reference);
    }

    @Override
    public Region findRegionByID(int id) {
        return dao.findRegionByID(id);
    }

    @Override
    public List<Region> listRegions(int firstPosition, int numberOfRecords) {
        return dao.listRegions(firstPosition, numberOfRecords);
    }

    @Override
    public boolean addRegion(Region region) {
        return dao.addRegion(region);
    }

    @Override
    public boolean removeRegion(Region region) {
        return dao.removeRegion(region);
    }

    @Override
    public boolean editRegion(Region region) {
        return dao.editRegion(region);
    }

    @Override
    public Ressource findRessourceByID(int id) {
        return dao.findRessourceByID(id);
    }

    @Override
    public List<Ressource> listRessources(int firstPosition, int numberOfRecords) {
        return dao.listRessources(firstPosition, numberOfRecords);
    }

    @Override
    public boolean addRessource(Ressource ressource) {
        return dao.addRessource(ressource);
    }

    @Override
    public boolean removeRessource(Ressource ressource) {
        return dao.removeRessource(ressource);
    }

    @Override
    public boolean editRessource(Ressource ressource) {
        return dao.editRessource(ressource);
    }

    @Override
    public RessourceType findRessourceTypeByID(int id) {
        return dao.findRessourceTypeByID(id);
    }

    @Override
    public List<RessourceType> listRessourceTypes(int firstPosition, int numberOfRecords) {
        return dao.listRessourceTypes(firstPosition, numberOfRecords);
    }

    @Override
    public boolean addRessourceType(RessourceType ressourceType) {
        return dao.addRessourceType(ressourceType);
    }

    @Override
    public boolean removeRessourceType(RessourceType ressourceType) {
        return dao.removeRessourceType(ressourceType);
    }

    @Override
    public boolean editRessourceType(RessourceType ressourceType) {
        return dao.editRessourceType(ressourceType);
    }

    @Override
    public Theme findThemeByID(int id) {
        return dao.findThemeByID(id);
    }

    @Override
    public List<Theme> listThemes(int firstPosition, int numberOfRecords) {
        return dao.listThemes(firstPosition, numberOfRecords);
    }

    @Override
    public boolean addTheme(Theme theme) {
        return dao.addTheme(theme);
    }

    @Override
    public boolean removeTheme(Theme theme) {
        return dao.removeTheme(theme);
    }

    @Override
    public boolean editTheme(Theme theme) {
        return dao.editTheme(theme);
    }

    @Override
    public List<Client> findClientByLastName(String name) {
        return dao.findClientByLastName(name);
    }

    @Override
    public List<Client> findClientByFirstName(String name) {
        return dao.findClientByFirstName(name);
    }

    @Override
    public List<Contact> findContactByLastName(String name) {
        return dao.findContactByLastName(name);
    }

    @Override
    public List<Contact> findContactByFirstName(String name) {
        return dao.findContactByFirstName(name);
    }

    @Override
    public List<Demande> findDemandeByDescription(String description) {
        return dao.findDemandeByDescription(description);
    }

    @Override
    public List<Document> findDocumentByTitle(String title) {
        return dao.findDocumentByTitle(title);
    }

    @Override
    public List<Document> findDocumentByDescription(String description) {
        return dao.findDocumentByDescription(description);
    }

    @Override
    public List<Intervention> findInterventionByDescription(String description) {
        return dao.findInterventionByDescription(description);
    }

    @Override
    public List<InterventionType> findInterventionTypeByLabel(String label) {
        return dao.findInterventionTypeByLabel(label);
    }

    @Override
    public List<Probleme> findProblemeByDescription(String description) {
        return dao.findProblemeByDescription(description);
    }

    @Override
    public List<Reference> findReferenceByDescription(String description) {
        return dao.findReferenceByDescription(description);
    }

    @Override
    public List<Region> findRegionByName(String name) {
        return dao.findRegionByName(name);
    }

    @Override
    public List<Ressource> findRessourceByLastName(String name) {
        return dao.findRessourceByFirstName(name);
    }

    @Override
    public List<Ressource> findRessourceByFirstName(String name) {
        return dao.findRessourceByFirstName(name);
    }

    @Override
    public List<RessourceType> findRessourceTypeByLabel(String label) {
        return dao.findRessourceTypeByLabel(label);
    }

    @Override
    public List<Theme> findThemeByLabel(String label) {
        return dao.findThemeByLabel(label);
    }
}

package DAL;

import DML.Client;
import DML.Contact;
import DML.Demande;
import DML.Document;
import DML.Intervention;
import DML.InterventionType;
import DML.Probleme;
import DML.Reference;
import DML.Region;
import DML.Ressource;
import DML.RessourceType;
import DML.Theme;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Alexis
 */
@Stateless
public class JpaDAO implements IDAO{
    
    @PersistenceContext(unitName = "CAAPAPU")
    private EntityManager em;

    @Override
    public Client findClientByID(int id) {
        Client value = em.find(Client.class, id);
        return value;
    }

    @Override
    public List<Client> listClients(int firstPosition, int numberOfRecords) {
        try{
            String sql = "SELECT t FROM client t";
            Query query = em.createQuery(sql);
            query.setFirstResult(firstPosition);
            query.setMaxResults(numberOfRecords);
            List result = query.getResultList();
            return result;
        }catch(Exception e){
            return null;
        }
    }

    @Override
    public boolean addClient(Client client) {
        try{
            
            em.persist(client);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean removeClient(Client client) {
        try{
            
            em.remove(client);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean editClient(Client client) {
        try{
            
            em.merge(client);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public Contact findContactByID(int id) {
        Contact value = em.find(Contact.class, id);
        return value;
    }

    @Override
    public List<Contact> listContacts(int firstPosition, int numberOfRecords) {
        try{
            String sql = "SELECT t FROM contact t";
            Query query = em.createQuery(sql);
            query.setFirstResult(firstPosition);
            query.setMaxResults(numberOfRecords);
            List result = query.getResultList();
            return result;
        }catch(Exception e){
            return null;
        }
    }

    @Override
    public boolean addContact(Contact contact) {
        try{
            
            em.persist(contact);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean removeContact(Contact contact) {
        try{
            
            em.remove(contact);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean editContact(Contact contact) {
        try{
            
            em.merge(contact);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public Demande findDemandeByID(int id) {
        Demande value = em.find(Demande.class, id);
        return value;
    }

    @Override
    public List<Demande> listDemandes(int firstPosition, int numberOfRecords) {
        try{
            String sql = "SELECT t FROM demande t";
            Query query = em.createQuery(sql);
            query.setFirstResult(firstPosition);
            query.setMaxResults(numberOfRecords);
            List result = query.getResultList();
            return result;
        }catch(Exception e){
            return null;
        }
    }

    @Override
    public boolean addDemande(Demande demande) {
        try{
            
            em.persist(demande);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean removeDemande(Demande demande) {
        try{
            
            em.remove(demande);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean editDemande(Demande demande) {
        try{
            
            em.merge(demande);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public Document findDocumentByID(int id) {
        Document value = em.find(Document.class, id);
        return value;
    }

    @Override
    public List<Document> listDocuments(int firstPosition, int numberOfRecords) {
        try{
            String sql = "SELECT t FROM document t";
            Query query = em.createQuery(sql);
            query.setFirstResult(firstPosition);
            query.setMaxResults(numberOfRecords);
            List result = query.getResultList();
            return result;
        }catch(Exception e){
            return null;
        }
    }

    @Override
    public boolean addDocument(Document document) {
        try{
            
            em.persist(document);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean removeDocument(Document document) {
        try{
            
            em.remove(document);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean editDocument(Document document) {
        try{
            
            em.merge(document);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public Intervention findInterventionByID(int id) {
        Intervention value = em.find(Intervention.class, id);
        return value;
    }

    @Override
    public List<Intervention> listInterventions(int firstPosition, int numberOfRecords) {
        try{
            String sql = "SELECT t FROM intervention t";
            Query query = em.createQuery(sql);
            query.setFirstResult(firstPosition);
            query.setMaxResults(numberOfRecords);
            List result = query.getResultList();
            return result;
        }catch(Exception e){
            return null;
        }
    }

    @Override
    public boolean addIntervention(Intervention intervention) {
        try{
            
            em.persist(intervention);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean removeIntervention(Intervention intervention) {
        try{
            
            em.remove(intervention);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean editIntervention(Intervention intervention) {
        try{
            
            em.merge(intervention);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public InterventionType findInterventionTypeByID(int id) {
        InterventionType value = em.find(InterventionType.class, id);
        return value;
    }

    @Override
    public List<InterventionType> listInterventionTypes(int firstPosition, int numberOfRecords) {
        try{
            String sql = "SELECT t FROM interventiontype t";
            Query query = em.createQuery(sql);
            query.setFirstResult(firstPosition);
            query.setMaxResults(numberOfRecords);
            List result = query.getResultList();
            return result;
        }catch(Exception e){
            return null;
        }
    }

    @Override
    public boolean addInterventionType(InterventionType interventionType) {
        try{
            
            em.persist(interventionType);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean removeInterventionType(InterventionType interventionType) {
        try{
            
            em.remove(interventionType);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean editInterventionType(InterventionType interventionType) {
        try{
            
            em.merge(interventionType);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public Probleme findProblemeByID(int id) {
        Probleme value = em.find(Probleme.class, id);
        return value;
    }

    @Override
    public List<Probleme> listProblemes(int firstPosition, int numberOfRecords) {
        try{
            String sql = "SELECT t FROM probleme t";
            Query query = em.createQuery(sql);
            query.setFirstResult(firstPosition);
            query.setMaxResults(numberOfRecords);
            List result = query.getResultList();
            return result;
        }catch(Exception e){
            return null;
        }
    }

    @Override
    public boolean addProbleme(Probleme probleme) {
        try{
            
            em.persist(probleme);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean removeProbleme(Probleme probleme) {
        try{
            
            em.remove(probleme);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean editProbleme(Probleme probleme) {
        try{
            
            em.merge(probleme);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public Reference findReferenceByID(int id) {
        Reference value = em.find(Reference.class, id);
        return value;
    }

    @Override
    public List<Reference> listReferences(int firstPosition, int numberOfRecords) {
        try{
            String sql = "SELECT t FROM reference t";
            Query query = em.createQuery(sql);
            query.setFirstResult(firstPosition);
            query.setMaxResults(numberOfRecords);
            List result = query.getResultList();
            return result;
        }catch(Exception e){
            return null;
        }
    }

    @Override
    public boolean addReference(Reference reference) {
        try{
            
            em.persist(reference);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean removeReference(Reference reference) {
        try{
            
            em.remove(reference);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean editReference(Reference reference) {
        try{
            
            em.merge(reference);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public Region findRegionByID(int id) {
        Region value = em.find(Region.class, id);
        return value;
    }

    @Override
    public List<Region> listRegions(int firstPosition, int numberOfRecords) {
        try{
            String sql = "SELECT t FROM region t";
            Query query = em.createQuery(sql);
            query.setFirstResult(firstPosition);
            query.setMaxResults(numberOfRecords);
            List result = query.getResultList();
            return result;
        }catch(Exception e){
            return null;
        }
    }

    @Override
    public boolean addRegion(Region region) {
        try{
            
            em.persist(region);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean removeRegion(Region region) {
        try{
            
            em.remove(region);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean editRegion(Region region) {
        try{
            
            em.merge(region);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public Ressource findRessourceByID(int id) {
        Ressource value = em.find(Ressource.class, id);
        return value;
    }

    @Override
    public List<Ressource> listRessources(int firstPosition, int numberOfRecords) {
        try{
            String sql = "SELECT t FROM ressource t";
            Query query = em.createQuery(sql);
            query.setFirstResult(firstPosition);
            query.setMaxResults(numberOfRecords);
            List result = query.getResultList();
            return result;
        }catch(Exception e){
            return null;
        }
    }

    @Override
    public boolean addRessource(Ressource ressource) {
        try{
            
            em.persist(ressource);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean removeRessource(Ressource ressource) {
        try{
            
            em.remove(ressource);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean editRessource(Ressource ressource) {
        try{
            
            em.merge(ressource);
            
            return true;
        }catch(Exception e){
            return false;
        }}

    @Override
    public RessourceType findRessourceTypeByID(int id) {
        RessourceType value = em.find(RessourceType.class, id);
        return value;
    }

    @Override
    public List<RessourceType> listRessourceTypes(int firstPosition, int numberOfRecords) {
        try{
            String sql = "SELECT t FROM ressourcetype t";
            Query query = em.createQuery(sql);
            query.setFirstResult(firstPosition);
            query.setMaxResults(numberOfRecords);
            List result = query.getResultList();
            return result;
        }catch(Exception e){
            return null;
        }
    }

    @Override
    public boolean addRessourceType(RessourceType ressourceType) {
        try{
            
            em.persist(ressourceType);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean removeRessourceType(RessourceType ressourceType) {
        try{
            
            em.remove(ressourceType);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean editRessourceType(RessourceType ressourceType) {
        try{
            
            em.merge(ressourceType);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public Theme findThemeByID(int id) {
        Theme value = em.find(Theme.class, id);
        return value;
    }

    @Override
    public List<Theme> listThemes(int firstPosition, int numberOfRecords) {
        try{
            String sql = "SELECT t FROM theme t";
            Query query = em.createQuery(sql);
            query.setFirstResult(firstPosition);
            query.setMaxResults(numberOfRecords);
            List result = query.getResultList();
            return result;
        }catch(Exception e){
            return null;
        }
    }

    @Override
    public boolean addTheme(Theme theme) {
        try{
            
            em.persist(theme);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean removeTheme(Theme theme) {
        try{
            
            em.remove(theme);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }

    @Override
    public boolean editTheme(Theme theme) {
        try{
            
            em.merge(theme);
            
            return true;
        }catch(Exception e){
            return false;
        }
    }
    
    @Override
    public List<RessourceType> findRessourceTypeByLabel(String label) {
        Query query = em.createQuery("SELECT r FROM ressourcetype r WHERE r.label LIKE :label");
        query.setParameter("label", "%"+label+"%");
        List<RessourceType> ressourceTypes = query.getResultList();
        return ressourceTypes;
    }
	
    @Override
    public List<Document> findDocumentByTitle(String title) {
        Query query = em.createQuery("SELECT d FROM document d WHERE d.title LIKE :title");
        query.setParameter("title", "%"+title+"%");
        List<Document> documents = query.getResultList();
        return documents;
    }
    
    @Override
        public List<Document> findDocumentByDescription(String description) {
        Query query = em.createQuery("SELECT d FROM document d WHERE d.description LIKE :description");
        query.setParameter("description", "%"+description+"%");
        List<Document> documents = query.getResultList();
        return null;
    }
	
    @Override
    public List<Theme> findThemeByLabel(String label) {
        Query query = em.createQuery("SELECT t FROM theme t WHERE t.label LIKE :label");
        query.setParameter("label", "%"+label+"%");
        List<Theme> themes = query.getResultList();
        return themes;
    }
	
    @Override
    public List<Probleme> findProblemeByDescription(String description) {
        Query query = em.createQuery("SELECT p FROM probleme p WHERE p.description LIKE :description");
        query.setParameter("description", "%"+description+"%");
        List<Probleme> problemes = query.getResultList();
        return problemes;
    }
	
    @Override
    public List<InterventionType> findInterventionTypeByLabel(String label) {
        Query query = em.createQuery("SELECT i FROM interventiontype i WHERE i.label LIKE :label");
        query.setParameter("label", "%"+label+"%");
        List<InterventionType> interventionType = query.getResultList();
        return interventionType;
    }
	
    @Override
    public List<Intervention> findInterventionByDescription(String description) {
        Query query = em.createQuery("SELECT i FROM intervention i WHERE i.description LIKE :description");
        query.setParameter("description", "%"+description+"%");
        List<Intervention> interventions = query.getResultList();
        return interventions;
    }
	
    @Override
    public List<Reference> findReferenceByDescription(String description) {
        Query query = em.createQuery("SELECT c FROM Reference c WHERE c.description LIKE :description");
        query.setParameter("description", "%"+description+"%");
        List<Reference> References = query.getResultList();
        return References;
    }
	
    @Override
    public List<Demande> findDemandeByDescription(String description) {
        Query query = em.createQuery("SELECT d FROM demande d WHERE d.description LIKE description");
        query.setParameter("description","%"+description+"%");
        List<Demande> demandes = query.getResultList();
        return demandes;
    }    

    @Override
    public List<Client> findClientByLastName(String name) {
        Query jpqlQuery = em.createQuery("SELECT c FROM client c JOIN c.contact co WHERE co.lastName LIKE :name ");
        jpqlQuery.setParameter("name", '%' + name + '%');
        List<Client> clients = (List<Client>) jpqlQuery.getResultList();
        return clients;
    }

    @Override
    public List<Client> findClientByFirstName(String name) {
        Query jpqlQuery = em.createQuery("SELECT c FROM client c JOIN c.contact co WHERE co.firstName LIKE :name ");
        jpqlQuery.setParameter("name", '%' + name + '%');
        List<Client> clients = (List<Client>) jpqlQuery.getResultList();
        return clients;
    }

    @Override
    public List<Contact> findContactByLastName(String name) {
        Query jpqlQuery = em.createQuery("SELECT c FROM contact c WHERE c.lastName LIKE :name ");
        jpqlQuery.setParameter("name", '%' + name + '%');
        List<Contact> contacts = (List<Contact>) jpqlQuery.getResultList();
        return contacts;
    }

    @Override
    public List<Contact> findContactByFirstName(String name) {
        Query jpqlQuery = em.createQuery("SELECT c FROM contact c WHERE c.firstName LIKE :name ");
        jpqlQuery.setParameter("name", '%' + name + '%');
        List<Contact> contacts = (List<Contact>) jpqlQuery.getResultList();
        return contacts;
    }

    @Override
    public List<Region> findRegionByName(String name) {
        Query jpqlQuery = em.createQuery("SELECT r FROM region r WHERE r.regionName LIKE :name");
        jpqlQuery.setParameter("name", "%"+name+"%");
        List<Region> regions = (List<Region>) jpqlQuery.getResultList();
        return regions;
    }

    @Override
    public List<Ressource> findRessourceByLastName(String name) {
        Query jpqlQuery = em.createQuery("SELECT r FROM ressource r JOIN r.contact c WHERE c.lastName LIKE :name ");
        jpqlQuery.setParameter("name", '%' + name + '%');
        List<Ressource> ressources = (List<Ressource>) jpqlQuery.getResultList();
        return ressources;
    }

    @Override
    public List<Ressource> findRessourceByFirstName(String name) {
        Query jpqlQuery = em.createQuery("SELECT r FROM ressource r JOIN r.contact c WHERE c.firstName LIKE :name ");
        jpqlQuery.setParameter("name", '%' + name + '%');
        List<Ressource> ressources = (List<Ressource>) jpqlQuery.getResultList();
        return ressources;
    }

}

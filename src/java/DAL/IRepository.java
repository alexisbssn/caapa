/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DML.*;
import java.util.List;

/**
 *
 * @author Pepe
 */
public interface IRepository {
    
    //client
    public Client findClientByID(int id);
    public List<Client> listClients(int firstPosition, int numberOfRecords);
    public boolean addClient(Client client);
    public boolean removeClient(Client client);
    public boolean editClient(Client client);
    
    //contact
    public Contact findContactByID(int id);
    public List<Contact> listContacts(int firstPosition, int numberOfRecords);
    public boolean addContact(Contact contact);
    public boolean removeContact(Contact contact);
    public boolean editContact(Contact contact);
    
    
    //demand
    public Demande findDemandeByID(int id);
    public List<Demande> listDemandes(int firstPosition, int numberOfRecords);
    public boolean addDemande(Demande demand);
    public boolean removeDemande(Demande demand);
    public boolean editDemande(Demande demand);
    
    //document
    public Document findDocumentByID(int id);
    public List<Document> listDocuments(int firstPosition, int numberOfRecords);
    public boolean addDocument(Document document);
    public boolean removeDocument(Document document);
    public boolean editDocument(Document document);
    
    //intervention
    public Intervention findInterventionByID(int id);
    public List<Intervention> listInterventions(int firstPosition, int numberOfRecords);
    public boolean addIntervention(Intervention intervention);
    public boolean removeIntervention(Intervention intervention);
    public boolean editIntervention(Intervention intervention);
    
    //intervention type
    public InterventionType findInterventionTypeByID(int id);
    public List<InterventionType> listInterventionTypes(int firstPosition, int numberOfRecords);
    public boolean addInterventionType(InterventionType interventionType);
    public boolean removeInterventionType(InterventionType interventionType);
    public boolean editInterventionType(InterventionType interventionType);
    
    //probleme
    public Probleme findProblemeByID(int id);
    public List<Probleme> listProblemes(int firstPosition, int numberOfRecords);
    public boolean addProbleme(Probleme probleme);
    public boolean removeProbleme(Probleme probleme);
    public boolean editProbleme(Probleme probleme);
    
    //reference
    public Reference findReferenceByID(int id);
    public List<Reference> listReferences(int firstPosition, int numberOfRecords);
    public boolean addReference(Reference reference);
    public boolean removeReference(Reference reference);
    public boolean editReference(Reference reference);
    
    //region
    public Region findRegionByID(int id);
    public List<Region> listRegions(int firstPosition, int numberOfRecords);
    public boolean addRegion(Region region);
    public boolean removeRegion(Region region);
    public boolean editRegion(Region region);
    
    //resource
    public Ressource findRessourceByID(int id);
    public List<Ressource> listRessources(int firstPosition, int numberOfRecords);
    public boolean addRessource(Ressource resource);
    public boolean removeRessource(Ressource resource);
    public boolean editRessource(Ressource resource);
    
    //resource type
    public RessourceType findRessourceTypeByID(int id);
    public List<RessourceType> listRessourceTypes(int firstPosition, int numberOfRecords);
    public boolean addRessourceType(RessourceType resourceType);
    public boolean removeRessourceType(RessourceType resourceType);
    public boolean editRessourceType(RessourceType resourceType);
    
    //theme
    public Theme findThemeByID(int id);
    public List<Theme> listThemes(int firstPosition, int numberOfRecords);
    public boolean addTheme(Theme theme);
    public boolean removeTheme(Theme theme);
    public boolean editTheme(Theme theme);

    public List<Client> findClientByLastName(String name);
    public List<Client> findClientByFirstName(String name);
    public List<Contact> findContactByLastName(String name);
    public List<Contact> findContactByFirstName(String name);
    public List<Demande> findDemandeByDescription(String description);
    public List<Document> findDocumentByTitle(String title);
    public List<Document> findDocumentByDescription(String description);
    public List<Intervention> findInterventionByDescription(String description);
    public List<InterventionType> findInterventionTypeByLabel(String label);
    public List<Probleme> findProblemeByDescription(String description);
    public List<Reference> findReferenceByDescription(String description);
    public List<Region> findRegionByName(String name);
    public List<Ressource> findRessourceByLastName(String name);
    public List<Ressource> findRessourceByFirstName(String name);
    public List<RessourceType> findRessourceTypeByLabel(String label);
    public List<Theme> findThemeByLabel(String label);
    
}

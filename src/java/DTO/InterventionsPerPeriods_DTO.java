/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.util.Date;

/**
 *
 * @author Pepe
 */
public class InterventionsPerPeriods_DTO {
    
    private int numberOfInterventions;
    private Date period;

    public InterventionsPerPeriods_DTO(int numberOfInterventions, Date period) {
        this.numberOfInterventions = numberOfInterventions;
        this.period = period;
    }

    public int getNumberOfInterventions() {
        return numberOfInterventions;
    }

    public void setNumberOfInterventions(int numberOfInterventions) {
        this.numberOfInterventions = numberOfInterventions;
    }

    public Date getPeriod() {
        return period;
    }

    public void setPeriod(Date period) {
        this.period = period;
    }
}

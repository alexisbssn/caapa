/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import DML.Theme;

/**
 *
 * @author Pepe
 */
public class InterventionsPerTheme_DTO {
    
    private int numberOfInterventions;
    private Theme theme;

    public InterventionsPerTheme_DTO(int numberOfInterventions, Theme theme) {
        this.numberOfInterventions = numberOfInterventions;
        this.theme = theme;
    }

    public int getNumberOfInterventions() {
        return numberOfInterventions;
    }

    public void setNumberOfInterventions(int numberOfInterventions) {
        this.numberOfInterventions = numberOfInterventions;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import DML.Region;

/**
 *
 * @author Pepe
 */
public class InterventionsPerRegion_DTO {
    
    private int numberOfInterventions;
    private Region region;

    public InterventionsPerRegion_DTO(int numberOfInterventions, Region region) {
        this.numberOfInterventions = numberOfInterventions;
        this.region = region;
    }

    public int getNumberOfInterventions() {
        return numberOfInterventions;
    }

    public void setNumberOfInterventions(int numberOfInterventions) {
        this.numberOfInterventions = numberOfInterventions;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }
    
}

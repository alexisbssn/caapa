/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import DML.Client;

/**
 *
 * @author jessyfortin
 */
public class Client_DTO {
    private Client client;
    private float hoursOfInterventions;
    private int numberOfInterventions;

    public Client_DTO(Client client, float hoursOfInterventions, int numberOfInterventions) {
        this.client = client;
        this.hoursOfInterventions = hoursOfInterventions;
        this.numberOfInterventions = numberOfInterventions;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public float getHoursOfInterventions() {
        return hoursOfInterventions;
    }

    public void setHoursOfInterventions(float hoursOfInterventions) {
        this.hoursOfInterventions = hoursOfInterventions;
    }

    public int getNumberOfInterventions() {
        return numberOfInterventions;
    }

    public void setNumberOfInterventions(int numberOfInterventions) {
        this.numberOfInterventions = numberOfInterventions;
    }
    
    
}
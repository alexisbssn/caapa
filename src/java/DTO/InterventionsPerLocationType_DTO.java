/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import DML.InterventionType;

/**
 *
 * @author Pepe
 */
public class InterventionsPerLocationType_DTO {
    
    private InterventionType interventionType;
    private int numberOfInterventions;

    public InterventionsPerLocationType_DTO(InterventionType interventionType, int numberOfInterventions) {
        this.interventionType = interventionType;
        this.numberOfInterventions = numberOfInterventions;
    }

    public InterventionType getInterventionType() {
        return interventionType;
    }

    public void setInterventionType(InterventionType interventionType) {
        this.interventionType = interventionType;
    }

    public int getNumberOfInterventions() {
        return numberOfInterventions;
    }

    public void setNumberOfInterventions(int numberOfInterventions) {
        this.numberOfInterventions = numberOfInterventions;
    }
    
    
}

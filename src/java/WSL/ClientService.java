package WSL;

import BLL.ClientController;
import DML.Client;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author alexis
 */

@Stateless
@Path("/client")
public class ClientService extends CrudService<Client>{

    @Override
    Client getById(int id) throws Exception {
        return ClientController.findClientByID(id);
    }

    @Override
    boolean add(Client obj) throws Exception {
        return ClientController.addClient(obj);
    }

    @Override
    boolean remove(int id) throws Exception {
        return ClientController.removeClient(new Client(id));
    }

    @Override
    boolean edit(Client obj) throws Exception {
        return ClientController.editClient(obj);
    }

    @Override
    List<Client> list(int firstPosition, int numberOfRecords) throws Exception {
        return ClientController.listClients(firstPosition, numberOfRecords);
    }
    
    @GET
    @Path("/{param}/demandes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDemandes(@PathParam("param") String id){
        try{
            int integerId = Integer.parseInt(id);
            
            Client obj = getById(integerId);
            if(obj != null){
                return Response.status(200).entity(gson.toJson(obj.getDemandes())).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).entity(gson.toJson("Id not found: " + id)).build();
            }
        }
        catch(NumberFormatException nfe){
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson(nfe)).build();
        }
        catch(Exception e){
            //Repository error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(gson.toJson("Internal server error")).build();
        }
    }
}

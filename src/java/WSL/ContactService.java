/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WSL;

import BLL.ContactController;
import DML.Contact;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 *
 * @author alexis
 */
@Stateless
@Path("/contact")
public class ContactService extends CrudService<Contact> {
    
    @Override
    Contact getById(int id) throws Exception {
        return ContactController.findContactByID(id);
    }

    @Override
    boolean add(Contact obj) throws Exception {
        return ContactController.addContact(obj);
    }

    @Override
    boolean remove(int id) throws Exception {
        return ContactController.removeContact(new Contact(id));
    }

    @Override
    boolean edit(Contact obj) throws Exception {
        return ContactController.editContact(obj);
    }

    @Override
    List<Contact> list(int firstPosition, int numberOfRecords) throws Exception {
        return ContactController.listContacts(firstPosition, numberOfRecords);
    }
}

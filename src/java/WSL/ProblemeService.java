/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WSL;

import BLL.ProblemController;
import DML.Probleme;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author alexis
 */
@Stateless
@Path("/probleme")
public class ProblemeService extends CrudService<Probleme> {
    
    @Override
    Probleme getById(int id) throws Exception {
        return ProblemController.findProblemeByID(id);
    }

    @Override
    boolean add(Probleme obj) throws Exception {
        return ProblemController.addProbleme(obj);
    }

    @Override
    boolean remove(int id) throws Exception {
        return ProblemController.removeProbleme(new Probleme(id));
    }

    @Override
    boolean edit(Probleme obj) throws Exception {
        return ProblemController.editProbleme(obj);
    }

    @Override
    List<Probleme> list(int firstPosition, int numberOfRecords) throws Exception {
        return ProblemController.listProblemes(firstPosition, numberOfRecords);
    }
    
    @GET
    @Path("/{param}/interventions")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInterventions(@PathParam("param") String id){
        try{
            int integerId = Integer.parseInt(id);
            
            Probleme obj = getById(integerId);
            if(obj != null){
                return Response.status(200).entity(gson.toJson(obj.getInterventions())).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).entity(gson.toJson("Id not found: " + id)).build();
            }
        }
        catch(NumberFormatException nfe){
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson(nfe)).build();
        }
        catch(Exception e){
            //Repository error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(gson.toJson("Internal server error")).build();
        }
    }
    
    
    @GET
    @Path("/{param}/problemes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProblemes(@PathParam("param") String id){
        try{
            int integerId = Integer.parseInt(id);
            
            Probleme obj = getById(integerId);
            if(obj != null){
                return Response.status(200).entity(gson.toJson(obj.getLinkedProblems())).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).entity(gson.toJson("Id not found: " + id)).build();
            }
        }
        catch(NumberFormatException nfe){
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson(nfe)).build();
        }
        catch(Exception e){
            //Repository error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(gson.toJson("Internal server error")).build();
        }
    }
    
    
    @GET
    @Path("/{param}/documents")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDocuments(@PathParam("param") String id){
        try{
            int integerId = Integer.parseInt(id);
            
            Probleme obj = getById(integerId);
            if(obj != null){
                return Response.status(200).entity(gson.toJson(obj.getDocuments())).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).entity(gson.toJson("Id not found: " + id)).build();
            }
        }
        catch(NumberFormatException nfe){
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson(nfe)).build();
        }
        catch(Exception e){
            //Repository error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(gson.toJson("Internal server error")).build();
        }
    }
}

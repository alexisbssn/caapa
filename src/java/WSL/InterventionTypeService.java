/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WSL;

import BLL.InterventionTypeController;
import DML.InterventionType;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 *
 * @author alexis
 */
@Stateless
@Path("/interventiontype")
public class InterventionTypeService extends CrudService<InterventionType>{
    
    @Override
    InterventionType getById(int id) throws Exception {
        return InterventionTypeController.findInterventionTypeByID(id);
    }

    @Override
    boolean add(InterventionType obj) throws Exception {
        return InterventionTypeController.addInterventionType(obj);
    }

    @Override
    boolean remove(int id) throws Exception {
        return InterventionTypeController.removeInterventionType(new InterventionType(id));
    }

    @Override
    boolean edit(InterventionType obj) throws Exception {
        return InterventionTypeController.editInterventionType(obj);
    }

    @Override
    List<InterventionType> list(int firstPosition, int numberOfRecords) throws Exception {
        return InterventionTypeController.listInterventionTypes(firstPosition, numberOfRecords);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WSL;

import BLL.ReferenceController;
import DML.Reference;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 *
 * @author alexis
 */
@Stateless
@Path("/reference")
public class ReferenceService extends CrudService<Reference> {
    
    @Override
    Reference getById(int id) throws Exception {
        return ReferenceController.findReferenceByID(id);
    }

    @Override
    boolean add(Reference obj) throws Exception {
        return ReferenceController.addReference(obj);
    }

    @Override
    boolean remove(int id) throws Exception {
        return ReferenceController.removeReference(new Reference(id));
    }

    @Override
    boolean edit(Reference obj) throws Exception {
        return ReferenceController.editReference(obj);
    }

    @Override
    List<Reference> list(int firstPosition, int numberOfRecords) throws Exception {
        return ReferenceController.listReferences(firstPosition, numberOfRecords);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WSL;

import BLL.DemandeController;
import DML.Demande;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author alexis
 */
@Stateless
@Path("/demande")
public class DemandeService extends CrudService<Demande> {
    @Override
    Demande getById(int id) throws Exception {
        return DemandeController.findDemandeByID(id);
    }

    @Override
    boolean add(Demande obj) throws Exception {
        return DemandeController.addDemande(obj);
    }

    @Override
    boolean remove(int id) throws Exception {
        return DemandeController.removeDemande(new Demande(id));
    }

    @Override
    boolean edit(Demande obj) throws Exception {
        return DemandeController.editDemande(obj);
    }

    @Override
    List<Demande> list(int firstPosition, int numberOfRecords) throws Exception {
        return DemandeController.listDemandes(firstPosition, numberOfRecords);
    }
    
    @GET
    @Path("/{param}/problemes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProblemes(@PathParam("param") String id){
        try{
            int integerId = Integer.parseInt(id);
            
            Demande obj = getById(integerId);
            if(obj != null){
                return Response.status(200).entity(gson.toJson(obj.getProblemes())).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).entity(gson.toJson("Id not found: " + id)).build();
            }
        }
        catch(NumberFormatException nfe){
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson(nfe)).build();
        }
        catch(Exception e){
            //Repository error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(gson.toJson("Internal server error")).build();
        }
    }
    
    @GET
    @Path("/{param}/interventions")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInterventions(@PathParam("param") String id){
        try{
            int integerId = Integer.parseInt(id);
            
            Demande obj = getById(integerId);
            if(obj != null){
                return Response.status(200).entity(gson.toJson(obj.getInterventions())).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).entity(gson.toJson("Id not found: " + id)).build();
            }
        }
        catch(NumberFormatException nfe){
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson(nfe)).build();
        }
        catch(Exception e){
            //Repository error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(gson.toJson("Internal server error")).build();
        }
    }
}

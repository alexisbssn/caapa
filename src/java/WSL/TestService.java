/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WSL;

import DAL.IRepository;
import DAL.Repository;
import DML.Client;
import DML.Contact;
import DML.Region;
import DML.Ressource;
import DML.RessourceType;
import java.sql.Date;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 *
 * @author alexis
 */
@Stateless
@Path("/test")
public class TestService {
    
    @GET
    public Response test() {

        String output = "Success";
            
        try {
            doTest();
        } catch (Exception ex) {
            output = "Failure: " + ex.getMessage() + "\n" + ex.getStackTrace();
        }

        return Response.status(200).entity(output).build();
    }
    
    public void doTest() throws Exception {
        IRepository instance = Repository.getInstance();
        
        RessourceType intervenant = new RessourceType("Intervenant");
        instance.addRessourceType(intervenant);
        RessourceType avocat = new RessourceType("Avocat");
        instance.addRessourceType(avocat);
        
        Region derriereLEtoile = new Region("Derriere l'étoile");
        instance.addRegion(derriereLEtoile);
        Region ileDeFrance = new Region("Ile de France");
        instance.addRegion(ileDeFrance);
        Region texas = new Region("Texas");
        instance.addRegion(texas);
        
        Ressource bianca = new Ressource("bianca","password",intervenant, derriereLEtoile, null, "Bianca","Castafiore","Rue du commisseriat","5145551234","", "bcastafiore@gmail.com");
        instance.addRessource(bianca);
        Ressource avocatBernard = new Ressource("bernard","password", avocat, ileDeFrance, null, "Kangourou","Bernard","Rue de la caserne","5145552345","5145553456","bkangourou@gmail.com");
        instance.addRessource(avocatBernard);
        
        Contact tinkerBell = new Contact("Bell","Tinker","Notre Dame de Paris","514-555-1234","","thinkerbell@gmail.com");
        instance.addContact(tinkerBell);
        
        Client peterPan = new Client("M", true, "Célibataire", new Date(2000,1,1), "Ile", null, derriereLEtoile, tinkerBell, null, "Pan","Peter","Pays imaginaire","514-555-1234","","peterpan@gmail.com");
        instance.addClient(peterPan);
        Client lostBoy = new Client("M", false, "Célibataire", new Date(2000,1,1), "Hotel", "House", texas, tinkerBell, null, "Luke","Lucky","Far west","514-555-1234","","luckyluke@gmail.com");
        instance.addClient(lostBoy);
        
        //List<Client> resultClient = instance.findClientByLastName(peterPan.getLastName());
        //List<Ressource> resultRessource = instance.findRessourceByLastName(avocatBernard.getLastName());
        
        //assertEquals(peterPan.getLastName(), resultClient.get(0).getLastName());
        //assertEquals(avocatBernard.getLastName(), resultRessource.get(0).getLastName());

        instance.removeClient(lostBoy);
        instance.removeClient(peterPan);
        instance.removeContact(tinkerBell);
        instance.removeRessource(avocatBernard);
        instance.removeRessource(bianca);
        instance.removeRegion(texas);
        instance.removeRegion(ileDeFrance);
        instance.removeRegion(derriereLEtoile);
        instance.removeRessourceType(avocat);
        instance.removeRessourceType(intervenant);
    }
}

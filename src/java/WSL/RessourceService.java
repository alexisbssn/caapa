/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WSL;

import BLL.RessourceController;
import DML.Ressource;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 *
 * @author alexis
 */
@Stateless
@Path("/ressource")
public class RessourceService extends CrudService<Ressource>{
    
    @Override
    Ressource getById(int id) throws Exception {
        return RessourceController.findRessourceByID(id);
    }

    @Override
    boolean add(Ressource obj) throws Exception {
        return RessourceController.addRessource(obj);
    }

    @Override
    boolean remove(int id) throws Exception {
        return RessourceController.removeRessource(new Ressource(id));
    }

    @Override
    boolean edit(Ressource obj) throws Exception {
        return RessourceController.editRessource(obj);
    }

    @Override
    List<Ressource> list(int firstPosition, int numberOfRecords) throws Exception {
        return RessourceController.listRessources(firstPosition, numberOfRecords);
    }
}

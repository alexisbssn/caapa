/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WSL;

import DAL.Repository;
import DML.Client;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import java.io.Serializable;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author alexis
 * @param <T> DML class this service acts as CRUD for
 */
public abstract class CrudService<T extends Serializable> {
    
    protected Gson gson = new Gson();
    
    abstract T getById(int id) throws Exception;
    abstract boolean add(T obj) throws Exception;
    abstract boolean remove(int id) throws Exception;
    abstract boolean edit(T obj) throws Exception;
    abstract List<T> list(int firstPosition, int numberOfRecords) throws Exception;
    
    @GET
    @Path("/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("param") String id){
        try{
            int integerId = Integer.parseInt(id);
            
            T obj = getById(integerId);
            if(obj != null){
                return Response.status(200).entity(gson.toJson(obj)).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).entity(gson.toJson("Id not found: " + id)).build();
            }
        }
        catch(NumberFormatException nfe){
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson(nfe)).build();
        }
        catch(Exception e){
            //Repository error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(gson.toJson("Internal server error")).build();
        }
    }
    
    @POST
    @Consumes("application/json")
    @Produces(MediaType.APPLICATION_JSON)
    public Response post(String json){
        try{
            T obj = gson.fromJson(json, new TypeToken<T>(){}.getType());
            if(add(obj) == true){
                return Response.status(200).entity(gson.toJson(obj)).build();
            }else{
                throw new Exception("Something went wrong");
            }
        }catch(JsonParseException e){
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson(e)).build();
        }catch(Exception e){
            //Repository error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(gson.toJson("Internal server error")).build();
        }
    }
    
    @PUT
    @Consumes("application/json")
    @Produces(MediaType.APPLICATION_JSON)
    public Response put(String json){
        
        // TODO add @Path("/{param}") and @PathParam("param") String id, to follow HTTP PUT standards. Problem right now is that type T does not necessarily have and id.
        //if(id != obj.id){
        //    return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson("Id in the path and the body don't match")).build();
        //}
        
        try{            
            T obj = gson.fromJson(json, new TypeToken<T>(){}.getType());
            
            if(edit(obj) == true){
                return Response.status(200).entity(gson.toJson(obj)).build();
            }else{
                throw new Exception("Something went wrong");
            }
        }catch(JsonParseException e){
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson(e)).build();
        }catch(Exception e){
            //Repository error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(gson.toJson("Internal server error")).build();
        }
    }
    
    @DELETE
    @Path("/{param}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("param") String id){
        try{
            int integerId = Integer.parseInt(id);
            
            if(remove(integerId) == true){
                return Response.status(200).entity(gson.toJson("Success")).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).entity(gson.toJson("Id not found: " + id)).build();
            }
        }
        catch(NumberFormatException nfe){
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson(nfe)).build();
        }
        catch(Exception e){
            //Repository error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(gson.toJson("Internal server error")).build();
        }
    }
    
    @GET
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
    public Response list(){
        try{
            List<T> obj = list(0, Integer.MAX_VALUE);
            return Response.status(200).entity(gson.toJson("Success")).build();
        }
        catch(Exception e){
            //Repository error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(gson.toJson("Internal server error")).build();
        }
    }
}

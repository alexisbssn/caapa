/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WSL;

import BLL.InterventionController;
import DML.Intervention;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author alexis
 */
@Stateless
@Path("/intervention")
public class InterventionService extends CrudService<Intervention>{
    
    @Override
    Intervention getById(int id) throws Exception {
        return InterventionController.findInterventionByID(id);
    }

    @Override
    boolean add(Intervention obj) throws Exception {
        return InterventionController.addIntervention(obj);
    }

    @Override
    boolean remove(int id) throws Exception {
        return InterventionController.removeIntervention(new Intervention(id));
    }

    @Override
    boolean edit(Intervention obj) throws Exception {
        return InterventionController.editIntervention(obj);
    }

    @Override
    List<Intervention> list(int firstPosition, int numberOfRecords) throws Exception {
        return InterventionController.listInterventions(firstPosition, numberOfRecords);
    }
    
    @GET
    @Path("/{param}/problemes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProblemes(@PathParam("param") String id){
        try{
            int integerId = Integer.parseInt(id);
            
            Intervention obj = getById(integerId);
            if(obj != null){
                return Response.status(200).entity(gson.toJson(obj.getProblemes())).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).entity(gson.toJson("Id not found: " + id)).build();
            }
        }
        catch(NumberFormatException nfe){
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson(nfe)).build();
        }
        catch(Exception e){
            //Repository error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(gson.toJson("Internal server error")).build();
        }
    }
    
    @GET
    @Path("/{param}/ressources")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getRessources(@PathParam("param") String id){
        try{
            int integerId = Integer.parseInt(id);
            
            Intervention obj = getById(integerId);
            if(obj != null){
                return Response.status(200).entity(gson.toJson(obj.getRessources())).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).entity(gson.toJson("Id not found: " + id)).build();
            }
        }
        catch(NumberFormatException nfe){
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson(nfe)).build();
        }
        catch(Exception e){
            //Repository error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(gson.toJson("Internal server error")).build();
        }
    }
}

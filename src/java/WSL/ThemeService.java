/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WSL;

import BLL.ThemeController;
import DML.Theme;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 *
 * @author alexis
 */
@Stateless
@Path("/theme")
public class ThemeService extends CrudService<Theme>{
    
    @Override
    Theme getById(int id) throws Exception {
        return ThemeController.findThemeByID(id);
    }

    @Override
    boolean add(Theme obj) throws Exception {
        return ThemeController.addTheme(obj);
    }

    @Override
    boolean remove(int id) throws Exception {
        return ThemeController.removeTheme(new Theme(id));
    }

    @Override
    boolean edit(Theme obj) throws Exception {
        return ThemeController.editTheme(obj);
    }

    @Override
    List<Theme> list(int firstPosition, int numberOfRecords) throws Exception {
        return ThemeController.listThemes(firstPosition, numberOfRecords);
    }
}

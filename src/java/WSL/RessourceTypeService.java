/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WSL;

import BLL.RessourceTypeController;
import DML.RessourceType;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 *
 * @author alexis
 */
@Stateless
@Path("/ressourcetype")
public class RessourceTypeService extends CrudService<RessourceType> {
    
    @Override
    RessourceType getById(int id) throws Exception {
        return RessourceTypeController.findRessourceTypeByID(id);
    }

    @Override
    boolean add(RessourceType obj) throws Exception {
        return RessourceTypeController.addRessourceType(obj);
    }

    @Override
    boolean remove(int id) throws Exception {
        return RessourceTypeController.removeRessourceType(new RessourceType(id));
    }

    @Override
    boolean edit(RessourceType obj) throws Exception {
        return RessourceTypeController.editRessourceType(obj);
    }

    @Override
    List<RessourceType> list(int firstPosition, int numberOfRecords) throws Exception {
        return RessourceTypeController.listRessourceTypes(firstPosition, numberOfRecords);
    }
}

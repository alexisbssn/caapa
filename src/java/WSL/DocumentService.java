/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WSL;

import BLL.DocumentController;
import DML.Document;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author alexis
 */
@Stateless
@Path("/document")
public class DocumentService extends CrudService<Document> {
    @Override
    Document getById(int id) throws Exception {
        return DocumentController.findDocumentByID(id);
    }

    @Override
    boolean add(Document obj) throws Exception {
        return DocumentController.addDocument(obj);
    }

    @Override
    boolean remove(int id) throws Exception {
        return DocumentController.removeDocument(new Document(id));
    }

    @Override
    boolean edit(Document obj) throws Exception {
        return DocumentController.editDocument(obj);
    }

    @Override
    List<Document> list(int firstPosition, int numberOfRecords) throws Exception {
        return DocumentController.listDocuments(firstPosition, numberOfRecords);
    }
    
    @GET
    @Path("/{param}/problemes")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getProblemes(@PathParam("param") String id){
        try{
            int integerId = Integer.parseInt(id);
            
            Document obj = getById(integerId);
            if(obj != null){
                return Response.status(200).entity(gson.toJson(obj.getLinkedProblemes())).build();
            }else{
                return Response.status(Response.Status.NOT_FOUND).entity(gson.toJson("Id not found: " + id)).build();
            }
        }
        catch(NumberFormatException nfe){
            return Response.status(Response.Status.BAD_REQUEST).entity(gson.toJson(nfe)).build();
        }
        catch(Exception e){
            //Repository error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(gson.toJson("Internal server error")).build();
        }
    }
}

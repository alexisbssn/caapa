/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WSL;

import BLL.RegionController;
import DML.Region;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.Path;

/**
 *
 * @author alexis
 */
@Stateless
@Path("/region")
public class RegionService extends CrudService<Region> {
    
    @Override
    Region getById(int id) throws Exception {
        return RegionController.findRegionByID(id);
    }

    @Override
    boolean add(Region obj) throws Exception {
        return RegionController.addRegion(obj);
    }

    @Override
    boolean remove(int id) throws Exception {
        return RegionController.removeRegion(new Region(id));
    }

    @Override
    boolean edit(Region obj) throws Exception {
        return RegionController.editRegion(obj);
    }

    @Override
    List<Region> list(int firstPosition, int numberOfRecords) throws Exception {
        return RegionController.listRegions(firstPosition, numberOfRecords);
    }
}

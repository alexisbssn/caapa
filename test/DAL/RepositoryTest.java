package DAL;

import DML.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Julien
 */
public class RepositoryTest {
    
    public RepositoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of the repository
     * Needs an empty database or it might fail.
     */
    @Test
    public void test() throws Exception {
        IRepository instance = Repository.getInstance();
        
        RessourceType intervenant = new RessourceType("Intervenant");
        instance.addRessourceType(intervenant);
        RessourceType avocat = new RessourceType("Avocat");
        instance.addRessourceType(avocat);
        
        Region derriereLEtoile = new Region("Derriere l'étoile");
        instance.addRegion(derriereLEtoile);
        Region ileDeFrance = new Region("Ile de France");
        instance.addRegion(ileDeFrance);
        Region texas = new Region("Texas");
        instance.addRegion(texas);
        
        Ressource bianca = new Ressource("bianca","password",intervenant, derriereLEtoile, null, "Bianca","Castafiore","Rue du commisseriat","5145551234","", "bcastafiore@gmail.com");
        instance.addRessource(bianca);
        Ressource avocatBernard = new Ressource("bernard","password", avocat, ileDeFrance, null, "Kangourou","Bernard","Rue de la caserne","5145552345","5145553456","bkangourou@gmail.com");
        instance.addRessource(avocatBernard);
        
        Contact tinkerBell = new Contact("Bell","Tinker","Notre Dame de Paris","514-555-1234","","thinkerbell@gmail.com");
        instance.addContact(tinkerBell);
        
        Client peterPan = new Client("Male", true, "Célibataire", new Date(2000,1,1), "Ile", null, derriereLEtoile, tinkerBell, null, "Pan","Peter","Pays imaginaire","514-555-1234","","peterpan@gmail.com");
        instance.addClient(peterPan);
        Client lostBoy = new Client("Male", false, "Célibataire", new Date(2000,1,1), "Hotel", "House", texas, tinkerBell, null, "Luke","Lucky","Far west","514-555-1234","","luckyluke@gmail.com");
        instance.addClient(lostBoy);
        
        List<Client> resultClient = instance.findClientByLastName(peterPan.getLastName());
        List<Ressource> resultRessource = instance.findRessourceByLastName(avocatBernard.getLastName());
        
        assertEquals(peterPan.getLastName(), resultClient.get(0).getLastName());
        assertEquals(avocatBernard.getLastName(), resultRessource.get(0).getLastName());

        instance.removeClient(lostBoy);
        instance.removeClient(peterPan);
        instance.removeContact(tinkerBell);
        instance.removeRessource(avocatBernard);
        instance.removeRessource(bianca);
        instance.removeRegion(texas);
        instance.removeRegion(ileDeFrance);
        instance.removeRegion(derriereLEtoile);
        instance.removeRessourceType(avocat);
        instance.removeRessourceType(intervenant);
    }
}

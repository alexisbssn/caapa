# CAAPA

* [AngularJS] - HTML enhanced for web apps!
* [Twitter Bootstrap] - great UI boilerplate for modern web apps

### Installation

TODO installation manual

* http://dillinger.io/ - Markdown (this file format) editor

### How to commit
faire du travail

`git status` n'importe quand pour voir on est rendu où

`git add *`

`git commit -m "mon message de commit"`

`git push`


### How to merge 
`git checkout master`

`git pull`

`git checkout user/monuser`

`git tag MyTag`
Tag example: alexis/remise1

`git push --tags`

`git reset --hard master`

`git push -f`


### How to get new modifications from master 
`git checkout master`

`git pull`

`git checkout user/monuser`

`git merge master`

`git status`

À ce moment, si il y a des conflits, me contacter.
Sinon, si le status indique `nothing to commit`, vous avez réussi.

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [AngularJS]: <http://angularjs.org>